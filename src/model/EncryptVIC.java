package model;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

public class EncryptVIC {

	public static int ID_LEN = 5; // # of chars in agent ID
	public static int DATE_LEN = 6; // # of chars in date
	public static int PHRASE_LEN = 10; // # letters of phrase to use
	public static int ANAGRAM_LEN = 10; // # of chars in anagram
	public static int ANAGRAM_LETTERS = 8; // # of letters in anagram

	public static char SPACE = (char) 32; // space is ASCII 32
	public static boolean DEBUG = false; // toggle debug prints

	/*=============================================================================
	 |   Assignment:  Program #[1]:  VIC Cipher
	 |       Author:  Michael Humphrey (michaeljhumphrey@email.arizona.edu)
	 |       Grader:  Lisa Peairs
	 |
	 |       Course:  CS345
	 |   Instructor:  L. McCann
	 |     Due Date:  September 17, 2013 at 14:00
	 |
	 |  Description:  This program reads from a file five pieces of information. Which are seperated with one
	 |				  on each line. These pieces of data will be used to create an encoded message using
	 |				  the vic cipher method (a little dumbed down). It does this be performing 8 steps of an algorithm which
	 |				  are described as follows. 
	 |				  1. Add the ID to the first five digits of the date, using no-carry addition
	 |				  2. Expand the 5-digit result of (1) to 10 digits, using chain addition.
	 |				  3. Use the phrase to create a digit permutation.
	 |				  4. Add the results of (2) and (3) using no-carry addition.
	 |				  5. Use the result from (4) to create a digit permutation
	 |				  6. Build a straddling checkerboard from (5)�s result and the anagram
	 |				  7. Use the straddling checkerboard to encode the message
	 |				  8. Insert the ID into the message.
	 |				  After these eight steps the program prints the message.
	 |                
	 | Deficiencies:  This program, after much testing works as was described in the
	 | 				  program handouT. There are no known errors.
	 *===========================================================================*/

	public static void main(String[] args) {
		for (String files : args) {
			long tempNum; // place holder for long additions
			String digitPerm; // first digit permutation in the algorithm
			ArrayList<String> alphabet = VICOperations.getAlphabet(); // sorted alphabet, used to locate correct location in checkerboard
			StringBuilder encryptedMessage = new StringBuilder(); // the final encrypted message
			int n; // n is the last digit of the date and will be where the agent id is inserted

			// Read the file for the needed info
			VICData vic = readVICData(files); // object that hold the data read from the file

			// 1. Add the ID to the first five digits of the date, using no-carry addition.
			tempNum = VICOperations.noCarryAddition(
					Long.parseLong(vic.agentID),
					Long.parseLong(vic.date.substring(0, 5)));

			// 2. Expand the 5-digit result of 1 to 10 digits, using chain addition.
			tempNum = VICOperations.chainAddition(tempNum, 10);

			// 3. Use the phrase to create a digit permutation
			digitPerm = VICOperations.digitPermutation(vic.phrase);

			// 4. Add the results of 2 and 3 using no-carry addition.
			tempNum = VICOperations.noCarryAddition(tempNum,
					Long.parseLong(digitPerm));

			// 5. Add the results of 2 and 3 using no-carry addition.
			String secondDigitPerm = VICOperations.digitPermutation(tempNum
					+ "");

			// 6. Build a straddling checkerboard from (5)�s result and the anagram.
			ArrayList<String> checkerboard = VICOperations
					.straddlingCheckerboard(secondDigitPerm, vic.anagram);

			// Look through the message one character at a time, find the matching character in the alphabet array, match
			// that index with the straddling checkerboard. The checker board is alphabetically ordered but lacks the actual letters
			// this pairing between the alphabet index and checkerboard will provide that and the code can be extracted.
			// 7. Use the straddling checkerboard to encode the message
			for (int i = 0; i < vic.message.length(); i++) {
				int indexOf = alphabet.indexOf(vic.message.charAt(i) + "");
				encryptedMessage.append(checkerboard.get(indexOf));
			}

			// 8. Insert the ID into the message
			n = Integer.parseInt(vic.date.substring(5));
			if ((n - 1) > encryptedMessage.length()) {
				encryptedMessage.append(vic.agentID);
			} else {
				encryptedMessage.insert(n, vic.agentID);
			}

			System.out.println(encryptedMessage.toString().trim());
		}
	}

	/*---------------------------------------------------------------------
	|  Method readVICData (pathName)
	|
	|  Purpose:  VIC requires five pieces of information for an
	|            encoding to be performed:  (1) agent ID (form: #####),
	|            (2) date (form:YYMMDD), (3) a phrase containing at least
	|            10 letters, (4) an anagram of 8 commonly-used letters
	|            and 2 spaces, and (5) a message of at least one letter
	|            to be encoded.  This method reads these pieces from the
	|            given filename (or path + filename), one per line, and
	|            all pieces are sanity-checked.  When reasonable,
	|            illegal characters are dropped.  The program is halted
	|            if an unrecoverable problem with the data is discovered.
	|
	|  Pre-condition:  None.  (We even check to see if the file exists.)
	|
	|  Post-condition:  The returned VICData object's fields are populated
	|            with legal data.
	|
	|  Parameters:  pathName -- The filename or path/filename of the text
	|            file containing the file pieces of data.  If only a
	|            file name is provided, it is assumed that the file
	|            is located in the same directory as the executable.
	|
	|  Returns:  A reference to an object of class VICData that contains
	|            the five pieces of data.
	 *-------------------------------------------------------------------*/

	public static VICData readVICData(String pathName) {
		VICData vic = new VICData(); // Object to hold the VIC input data
		Scanner inFile = null; // Scanner file object reference

		try {
			inFile = new Scanner(new File(pathName));
		} catch (Exception e) {
			System.out.println("File does not exist: " + pathName + "!\n");
			System.exit(1);
		}

		// Read and sanity-check agent ID.  Needs to be ID_LEN long
		// and numeric.

		if (inFile.hasNext()) {
			vic.agentID = inFile.nextLine();
		} else {
			System.out.println("ERROR:  Agent ID not found!\n");
			System.exit(1);
		}

		if (vic.agentID.length() != ID_LEN) {
			System.out.printf("ERROR:  Agent ID length is %d, must be %d!\n",
					vic.agentID.length(), ID_LEN);
			System.exit(1);
		}

		try {
			long idValue = Long.parseLong(vic.agentID);
		} catch (NumberFormatException e) {
			System.out.println("Agent ID `" + vic.agentID
					+ "contains non-numeric characters!\n");
			System.exit(1);
		}

		// Read and sanity-check date.  Needs to be DATE_LEN long
		// and numeric.

		if (inFile.hasNext()) {
			vic.date = inFile.nextLine();
		} else {
			System.out.println("ERROR:  Date not found!\n");
			System.exit(1);
		}

		if (vic.date.length() != DATE_LEN) {
			System.out.printf("ERROR:  Date length is %d, must be %d!\n",
					vic.date.length(), DATE_LEN);
			System.exit(1);
		}

		try {
			long dateValue = Long.parseLong(vic.date);
		} catch (NumberFormatException e) {
			System.out.println("Date `" + vic.date
					+ "contains non-numeric characters!\n");
			System.exit(1);
		}

		// Read and sanity-check phrase.  After removing non-letters,
		// at least PHRASE_LEN letters must remain.

		if (inFile.hasNext()) {
			vic.phraseOriginal = inFile.nextLine();
			StringBuffer sb = new StringBuffer(vic.phraseOriginal);
			for (int i = 0; i < sb.length(); i++) {
				if (!Character.isLetter(sb.charAt(i))) {
					sb.deleteCharAt(i);
					i--; // Don't advance to next index o.w. we miss a char
				}
			}
			vic.phrase = sb.toString().toUpperCase();
			if (vic.phrase.length() >= PHRASE_LEN) {
				vic.phrase = vic.phrase.substring(0, PHRASE_LEN);
			}
		} else {
			System.out.println("ERROR:  Phrase not found!\n");
			System.exit(1);
		}

		if (vic.phrase.length() != PHRASE_LEN) {
			System.out.printf("ERROR:  Phrase contains %d letter(s), "
					+ "must have at least %d!\n", vic.phrase.length(),
					PHRASE_LEN);
			System.exit(1);
		}

		// Read and sanity-check anagram.  Must be ANAGRAM_LEN long,
		// and contain ANAGRAM_LETTERS letters and the rest spaces.

		if (inFile.hasNext()) {
			vic.anagram = inFile.nextLine().toUpperCase();
		} else {
			System.out.println("ERROR:  Anagram not found!\n");
			System.exit(1);
		}

		if (vic.anagram.length() != ANAGRAM_LEN) {
			System.out.printf("ERROR:  Anagram length is %d, must be %d!\n",
					vic.anagram.length(), ANAGRAM_LEN);
			System.exit(1);
		}

		for (int i = 0; i < vic.anagram.length(); i++) {
			if (!Character.isLetter(vic.anagram.charAt(i))
					&& vic.anagram.charAt(i) != SPACE) {
				System.out.printf("ERROR:  Anagram contains character `%c'.\n",
						vic.anagram.charAt(i));
				System.exit(1);
			}
		}

		int numLetters = 0;
		for (int i = 0; i < vic.anagram.length(); i++) {
			if (Character.isLetter(vic.anagram.charAt(i))) {
				numLetters++;
			}
		}
		if (numLetters != ANAGRAM_LETTERS) {
			System.out.printf("ERROR:  Anagram contains %d letters, "
					+ "should have %d plus %d spaces.\n", numLetters,
					ANAGRAM_LETTERS, ANAGRAM_LEN - ANAGRAM_LETTERS);
			System.exit(1);
		}

		// Read and sanity-check message.  After removing non-letters
		// and capitalizing, at least one letter must remain.

		if (inFile.hasNext()) {
			vic.messageOriginal = inFile.nextLine();
			StringBuffer sb = new StringBuffer(vic.messageOriginal);
			for (int i = 0; i < sb.length(); i++) {
				if (!Character.isLetter(sb.charAt(i))) {
					sb.deleteCharAt(i);
					i--; // Don't advance to next index o.w. we miss a char
				}
			}
			vic.message = sb.toString().toUpperCase();
		} else {
			System.out.println("ERROR:  Message not found!\n");
			System.exit(1);
		}

		if (vic.message.length() == 0) {
			System.out.printf("ERROR:  Message contains no letters!\n");
			System.exit(1);
		}

		if (DEBUG) {
			System.out.printf("vic.agentID = %s\n", vic.agentID);
			System.out.printf("vic.date = %s\n", vic.date);
			System.out.printf("vic.phrase = %s\n", vic.phrase);
			System.out.printf("vic.anagram = %s\n", vic.anagram);
			System.out.printf("vic.message = %s\n", vic.message);
		}

		return vic;
	}

	private static class VICData {

		public String agentID; // stores the agent ID read from the file
		public String date; // stores the date read from the file
		public String phrase; // stores the phrase read from the file
		public String phraseOriginal;
		public String anagram; // stores the anagram read from the file
		public String message; // stores the message read from the file
		public String messageOriginal;
	}
	
} // end class EncryptVIC
