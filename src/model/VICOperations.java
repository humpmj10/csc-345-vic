package model;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

/*+----------------------------------------------------------------------
 ||
 ||  Class VICOperations
 ||
 ||         Author:  Michael Humphrey
 ||
 ||        Purpose:  This class handles the mathematical operations for the Decode/Encode classes.
 ||                  It has been separated from those classes to produce less code. It will perform noCarryAddition, chainAddition, digitPermuation
 ||					 and finally create a straddlingCheckerboard which are commonly used in ciphers.
 ||
 ||  Inherits From:  None.
 ||                  
 ||
 ||     Interfaces:  None.
 ||                   
 ||
 |+-----------------------------------------------------------------------
 ||
 ||      Constants:  String basdir, used to ensure the correct file separators are used depending on the os the program is ran on
 ||
 |+-----------------------------------------------------------------------
 ||
 ||   Constructors:  None
 ||
 ||  Class Methods:  noCarryAddition(long firstAddend, long secondAddend): long
 ||					 chaingAddition(long startNumber, int length): long
 ||					 digitPermutation(String startingString): String
 ||					 straddlingCheckerBoard(String digitPerm, String anagram): ArrayList
 ||					 getAlphabet(): ArrayList
 ||
 ||  Inst. Methods:  None
 ||
 ++-----------------------------------------------------------------------*/

public class VICOperations {

	private static String baseDir = System.getProperty("user.dir")
			+ File.separator + "docs" + File.separator; // ensures that program can locate the file on the disk regardless of which os it's being run on

	/*---------------------------------------------------------------------
	|  Method noCarryAddition
	|
	|  Purpose: Part of the VIC encryption algorithm requires no carry addition. No carry addition is exactly as it sounds.   
	|      		When the addition is done, instead of carrying over the digit, it's discarded.
	|			For example,  359 + 163 normally equals 522, but in no-carry addition 359 + 163 = 412. 
	|			The encryption and decryption class will call this method to perform this operation.
	|
	|  Pre-condition: Both firstAddend and secondAddend must be positive for this method to function correctly.3
	|	   Any non-obvious conditions that must exist
	|      or be true before we can expect this method to function
	|      correctly.]
	|
	|  Post-condition: [What we can expect to exist or be true after
	|      this method has executed under the pre-condition(s).]
	|
	|  Parameters:
	|		firstAddend -- This is the first number for the no-carry addition operation.
	|
	|		secondAddend -- This is the second number for the no-carry addition operation.
	|
	|  Returns:  Returns the no carry addition of numberOne and numberTwo as a long. This value will be used by
	|			 the class to perform the encryption/decryption.
	|      
	|      
	 *-------------------------------------------------------------------*/
	public static long noCarryAddition(long firstAddend, long secondAddend) {
		// if both numbers are 0 just return 0
		if (firstAddend == 0 && secondAddend == 0) {
			return 0;
		}

		ArrayList<Long> firstNumber = new ArrayList<Long>(); // Will contain the first long param broken into digits
		ArrayList<Long> secondNumber = new ArrayList<Long>(); // Will contain the second long param broken into digits
		ArrayList<Long> result = new ArrayList<Long>(); // Will contain the result of the no carry addition
		long tempNumOne; // Place holder for each digit take from firstNumber, used in calculation
		long tempNumTwo; // Place holder for each digit take from secondNumber, used in calculation
		long tempOperatedNum; // Place holder for each resulting digit in the no carry addition
		StringBuilder resultString = new StringBuilder(); // The final return value

		// The following two while loops break each of the addends
		// into digits and stores them in the appropriate arrayList
		while (firstAddend > 0) {
			firstNumber.add(firstAddend % 10);
			firstAddend = firstAddend / 10;
		}
		while (secondAddend > 0) {
			secondNumber.add(secondAddend % 10);
			secondAddend = secondAddend / 10;
		}
		/*----------------------------------------------------------------
		  |The following checks to see which number is bigger so that the for loop will iterate 
		  | over that array and substitute zero in when the smaller number has 
		  | run out of digits. The no carry addition is accomplished by adding the 
		  | two digits and then using mod 10 to only keep the last digit and store it in the result arrayList.
		  | The result array holds the number but in reverse order.
		 -----------------------------------------------------------------*/
		if (firstNumber.size() > secondNumber.size()) {
			for (int i = 0; i < firstNumber.size(); i++) {
				tempNumOne = firstNumber.get(i);
				if (i >= secondNumber.size()) {
					tempNumTwo = 0;
				} else {
					tempNumTwo = secondNumber.get(i);
				}
				tempOperatedNum = ((tempNumOne + tempNumTwo) % 10);
				result.add(tempOperatedNum);
			}
		} else {
			for (int i = 0; i < secondNumber.size(); i++) {
				tempNumTwo = secondNumber.get(i);
				if (i >= firstNumber.size()) {
					tempNumOne = 0;
				} else {
					tempNumOne = firstNumber.get(i);
				}
				tempOperatedNum = ((tempNumOne + tempNumTwo) % 10);
				result.add(tempOperatedNum);
			}
		}

		// Use string builder to insert each element in the first string position
		// to reverse the number back to the correct order since it was stored in reverse order
		for (long num : result) {
			resultString.insert(0, Long.toString(num));
		}

		return Long.parseLong(resultString.toString().trim());

	}

	/*---------------------------------------------------------------------
	|  Method chainAddition
	|
	|  Purpose: This method will create a longer number by performing no carry addition on two digits at a time and appending
	|       	the created digit to the existing number. The method will sequentially perform this operation on sequential pairs until
	|			the length of parameter two has been reached. This chainAddition is another operation required for the VIC
	|			algorithm.
	|
	|  Pre-condition: Assumes no length of zero will be inputed.
	|	   [Any non-obvious conditions that must exist
	|      or be true before we can expect this method to function
	|      correctly.]
	|
	|  Post-condition: [What we can expect to exist or be true after
	|      this method has executed under the pre-condition(s).]
	|
	|  Parameters:
	|		startNumber -- This parameter is the starting number and will be extended to the length parameter
	|
	|		length -- This is the length of startNumber that the method will lengthen startNumber too.
	|
	|
	|  Returns:  This method returns a long which is the original number lengthened. 
	|     
	 *-------------------------------------------------------------------*/
	public static long chainAddition(long startNumber, int length) {
		ArrayList<Long> result = new ArrayList<Long>(); // This will contain the result with each element as one char of the final string
		String resultString = ""; // This is the return value

		// Breakdown startNumber into digits and store in arrayList
		while (startNumber > 0) {
			result.add(0, startNumber % 10);
			startNumber = startNumber / 10;
		}

		// If length is smaller than or equal to startNumber return the truncated number.
		if (result.size() >= length) {
			for (int i = 0; i < length; i++) {
				resultString += result.get(i);
			}
			return Long.parseLong(resultString);
		}

		//  If startNumber is one digit need to use 0 as the first digit in the no carry addition
		//  Left outside the loop so that it doesn't have to be check every iteration
		if (result.size() == 1) {
			result.add(VICOperations.noCarryAddition(0, result.get(0)));
		}

		int j = 0; // used to sequential beginning pairs in the arrayList
		for (int i = result.size(); i < length; i++) {
			long temp = VICOperations.noCarryAddition(result.get(j),
					result.get(j + 1));
			result.add(temp);
			j++;
		}

		// Reassembles the digits as a String
		for (long digit : result) {
			resultString += digit;
		}
		// Turn the final String back into a long and return
		return Long.parseLong(resultString);
	}

	/*---------------------------------------------------------------------
	|  Method 
	|
	|  Purpose: This method takes a String and assigns the numbers 0-9, it does this alphabetically and sequentially.
	|			An example of assignment: B A N A N A L A N D   B A N A N A L A N D   B A N A N A L A N D  
	|										0   1   2   3 	    4 0   1   2   3   5   4 0 7 1 8 2 6 3 9 5 	
	|			So the resulting number is 4071826395. The method will store the original String and a sorted String in two
	|			ArrayList, it will then assign 0-9 to the sorted ArrayList. The method match the letters from these two ArrayList
	|			to created the final String sequence of digits and return this. 
	|      
	|
	|  Pre-condition:  [Any non-obvious conditions that must exist
	|      or be true before we can expect this method to function
	|      correctly.]
	|
	|  Post-condition: [What we can expect to exist or be true after
	|      this method has executed under the pre-condition(s).]
	|
	|  Parameters:
	|	   startingString -- This is the string that will used to create the 10 digit return number
	|
	|  Returns:  Method returns a string of 10 digits which will be used in the VIC algorithm of encoding.
	|    
	|      
	 *-------------------------------------------------------------------*/
	public static String digitPermutation(String startingString) {
		String result = ""; // This will be the final return value

		// Check initial condition that the string be greater than 9 characters
		if (startingString.length() < 10)
			return null;
		else {

			List<String> unsortedString = new ArrayList<String>(); // Will contain the unsorted string with one letter as each element
			List<String> sortedString = new ArrayList<String>(); // Will contain the sorted string with one letter as each element

			// Break apart startingString and store each letter in arrayList, First letter will correspond with array[0]
			while (!startingString.equals("")) {
				unsortedString.add("" + startingString.toUpperCase().charAt(0));
				startingString = startingString.substring(1).trim();
			}

			// Creates the sorted arrayList with startingString and keeps unsortedString unsorted
			List<String> tempList = new ArrayList<String>();
			tempList.addAll(unsortedString); // Needed to keep a copy of the original unsorted string
			Collections.sort(unsortedString);
			sortedString = unsortedString;
			unsortedString = tempList;

			// This loop searches iterates through sortedString, it looks for each letter in the 
			// the unsorted arrayList, it then appends that number to the result string.
			for (String letter : unsortedString) {
				result += "" + sortedString.indexOf(letter);
				sortedString.set(sortedString.indexOf(letter), "null");
			}

		}
		return result;

	}

	/*---------------------------------------------------------------------
	|  Method straddlingCheckerboard
	|
	|  Purpose: This method uses a digit permutation and anagram to create values for each letter of the alphabet.
	|			The method creates an array that will contain the digit permutation from 0-9 as it's first row followed by
	|			the anagram which contains two spaces. The spaces will be used as the first digit in the code for rows 2 and 3.
	|			This 2d matrix is being represented as a standard 1-d array, each row will contain 10 elements.
	|			Once each letter is assigned it's code, the method will return a sorted arrayList containing these codes.
	|
	|      
	|
	|  Pre-condition: Both the anagram and digit permutation must contain exactly 10 characters. The anagram may not contain any digits and
	|				  the permutation may not contain any letters. There is error checking for these errors, however the method
	|				  will only return null and not return a reason.
	|
	|  Post-condition: The array list will contain 26 unique strings.
	|
	|  Parameters:
	|		anagram --  This is the structure used to create a piece of the checker board.
	|					It will include two spaces for the gaps needed to create the next two rows. 
	|
	|		digitPerm -- This is the number sequence used to map the letters, two will be taken and added 
	|					 to create two more row indexes.
	|
	|
	|  Returns: An arrayList of strings, the strings will act as number codes for our cipher.
	|			Each index from 0-25 will correspond to a letter in the alphabet A-Z. 
	|
	 *-------------------------------------------------------------------*/
	public static ArrayList<String> straddlingCheckerboard(String digitPerm,
			String anagram) {

		ArrayList<String> result = new ArrayList<String>(); // the result arrayList which will be returned
		ArrayList<String> digitPermCharacters = new ArrayList<String>(); // will store each character of thedigitPerm string in a separate element
		ArrayList<String> anagramCharacters = new ArrayList<String>(); // will store each character of the anagram string in a separate element
		ArrayList<String> characterTable = new ArrayList<String>(); // will store the followed by the anagram and then the remaining alphabet
		ArrayList<String> alphabet = VICOperations.getAlphabet(); // stores the alphabet sorted and Uppercase

		int numberOfSpaces = 0; // used to track the number of spaces and ensure the anagram contains exactly 2
		String blankIndex = null; // stores the first blank index in the anagram
		String secondBlankIndex = null; // stores the second blank index in the anagram

		// Check length of both String params
		if (digitPerm.length() != 10 || anagram.length() != 10) {
			return null;
		}
		// Test to see if either String argument contains invalid characters, anagram should only contain letters
		// and digitPerm should only contain numbers
		if (digitPerm.matches(".*[a-zA-Z].*") || anagram.matches(".*[0-9].*")) {
			return null;
		}

		// Break each params into one characters strings and stores each in 
		// a separate array. Each character is stored as uppercase.
		for (int i = 0; i < 10; i++) {
			if (digitPermCharacters.contains(digitPerm.charAt(i) + "")) {
				return null;
			}
			digitPermCharacters.add(digitPerm.charAt(i) + "");
			anagramCharacters.add(anagram.toUpperCase().charAt(i) + "");
		}

		// Make sure that anagram contains exactly two spaces
		for (String str : anagramCharacters) {
			if (str.equals(" ")) {
				numberOfSpaces++;
			}
		}
		if (numberOfSpaces != 2) {
			return null;
		}

		// The following three loops will add the digits of the permutation, the chars of the anagram
		// and then will fill in the remaining characters of the alphabet
		for (String str : digitPermCharacters) {
			characterTable.add(str);
		}
		for (String str : anagramCharacters) {
			characterTable.add(str);
		}
		for (String str : alphabet) {
			if (!characterTable.contains(str))
				characterTable.add(str);
		}

		// Table is now loaded, need to make an equation to calculate the value
		int j = 0; // used to find the index on the digit perm, will be mod 10 so it can repeat for the next row
		int i = 1; // used to indicate which row the loop is on
		for (int k = 10; k < characterTable.size(); k++) {
			if (i == 2) { // i = 2 means all letters will contain the first blank index followed by the digit perm index
				result.add(characterTable.get(k) + " " + blankIndex
						+ characterTable.get(j % 10));
			} else if (i == 3) {
				result.add(characterTable.get(k) + " " + secondBlankIndex
						+ characterTable.get(j % 10));
			}

			else { // i = 2 means all letters will contain the second blank index followed by the digit perm index
				if (characterTable.get(k).equals(" ") && blankIndex == null) {
					blankIndex = characterTable.get(j);
				} else if (characterTable.get(k).equals(" ")
						&& blankIndex != null) {
					secondBlankIndex = characterTable.get(j);
				} else
					result.add(characterTable.get(k) + " "
							+ characterTable.get(j));
			}
			j++;
			if ((j != 0) && (j % 10 == 0)) {
				i++;
			}

		}
		Collections.sort(result);
		for (int p = 0; p < result.size(); p++) {
			result.set(p, result.get(p).substring(2));
		}
		return result;
	}

	/*---------------------------------------------------------------------
	|  Method getAlphabet
	|
	|  Purpose: This method reads the alphabet from a file and stores it in an arrayList. This method
	|			is needed in the straddlingCheckerboard method because the arrayList only stores the code
	|			it needs a reference for the position in the alphabet. The index from the alphabet array is matched
	|			to the array in straddlingCheckerboard.
	|
	|  Pre-condition: The file must exist for this method to function.
	|
	|  Post-condition: The arraylist will contain 26 elements containing the sorted uppercase alphabet
	|
	|  Parameters:
	|     None
	|
	|  Returns: returns an arraylist that contaisn 26 elements containing the sorted uppercase alphabet
	 *-------------------------------------------------------------------*/
	public static ArrayList<String> getAlphabet() {
		ArrayList<String> result = new ArrayList<String>(); // stores the resulting alphabet array

		// load from a file the alphabet
		String file = "alphabet.txt"; // stores the file name 
		Scanner scan;
		try {
			scan = new Scanner(new File(baseDir + file));
			while (scan.hasNext()) {
				result.add(scan.next());
			}
			scan.close();
		} catch (FileNotFoundException e) {

		}
		return result;
	}

} // end class VICOperations
