package tests;

import static org.junit.Assert.assertEquals;

import static org.junit.Assert.assertNull;

import java.util.ArrayList;

import model.VICOperations;

import org.junit.Test;

/**
 * This is the test class for VICOperations. It test the four operations
 * noCarryAddition, chainAddition, digitPermuation and straddlingCheckerBoard.
 * 
 * @author Michael Humphrey
 * 
 */
public class VICOperationsTest {

	/**
	 * Tests the noCarryAddition, test for different combinations of numbers to
	 * ensure that the method is performing properly.
	 */
	@Test
	public void testNoCarryAddition() {

		assertEquals(15, VICOperations.noCarryAddition(7L, 18L));
		assertEquals(15, VICOperations.noCarryAddition(18L, 7L));
		assertEquals(412, VICOperations.noCarryAddition(359L, 163L));
		assertEquals(412, VICOperations.noCarryAddition(163L, 359L));
		assertEquals(3, VICOperations.noCarryAddition(1L, 2L));
		assertEquals(3, VICOperations.noCarryAddition(2L, 1L));
		assertEquals(9, VICOperations.noCarryAddition(9L, 0L));
		assertEquals(9, VICOperations.noCarryAddition(0L, 9L));
		assertEquals(0, VICOperations.noCarryAddition(1L, 9L));
		assertEquals(0, VICOperations.noCarryAddition(9L, 1L));
		assertEquals(70, VICOperations.noCarryAddition(9L, 71L));
		assertEquals(70, VICOperations.noCarryAddition(71L, 9L));
		assertEquals(1000, VICOperations.noCarryAddition(1111L, 999L));
		assertEquals(1000, VICOperations.noCarryAddition(999L, 1111L));
		assertEquals(0, VICOperations.noCarryAddition(0L, 0L));
		assertEquals(0, VICOperations.noCarryAddition(11111111L, 99999999L));
		assertEquals(0, VICOperations.noCarryAddition(99999999L, 11111111L));
	}

	/**
	 * Test will ensure that when a length parameter that is smaller than the
	 * starting number is used the method returns a truncated starting number or
	 * the number itself in the case that starting length = desired length.
	 */
	@Test
	public void testChainAdditionLengthLessThanStart() {
		assertEquals(2L, VICOperations.chainAddition(22342342L, 1));
		assertEquals(22L, VICOperations.chainAddition(22342342L, 2));
		assertEquals(223L, VICOperations.chainAddition(22342342L, 3));
		assertEquals(2234L, VICOperations.chainAddition(22342342L, 4));
		assertEquals(22342L, VICOperations.chainAddition(22342342L, 5));
		assertEquals(223423L, VICOperations.chainAddition(22342342L, 6));
		assertEquals(2234234L, VICOperations.chainAddition(22342342L, 7));
		assertEquals(22342342L, VICOperations.chainAddition(22342342L, 8));
	}

	/**
	 * Test the Chain addition for the case when starting number only has one
	 * digit. The method should use 0 as the first addend and the first digit as
	 * the second.
	 */
	@Test
	public void testChainAdditionStartingOneDigit() {
		assertEquals(224L, VICOperations.chainAddition(2L, 3));
		assertEquals(77415L, VICOperations.chainAddition(7L, 5));
		assertEquals(998L, VICOperations.chainAddition(9L, 3));
		assertEquals(54L, VICOperations.chainAddition(54321L, 2));
	}

	/**
	 * Test chain addition method for various lengths with greater than 1
	 * starting number.
	 */
	@Test
	public void testChainAdditionMoreThanOneStarting() {
		assertEquals(76238513L, VICOperations.chainAddition(762L, 8));
		assertEquals(2271249836L, VICOperations.chainAddition(22712L, 10));
	}

	/**
	 * Test digitPermutation with upper and lower case strings, tests with
	 * digits and string shorter than 10.
	 */

	@Test
	public void testDigitPermutation() {
		assertEquals("4071826395", VICOperations.digitPermutation("BANANALAND"));
		assertEquals("4071826395", VICOperations.digitPermutation("BaNaNaLaNd"));
		assertEquals("4071826395", VICOperations.digitPermutation("BANANALANd"));
		assertEquals("6704821539", VICOperations.digitPermutation("STARTEARLY"));
		assertEquals("6704821539", VICOperations.digitPermutation("STARTEARLY"));
		assertNull(VICOperations.digitPermutation("STARTEARL"));
		assertNull(VICOperations.digitPermutation("STARTEAR"));
		assertNull(VICOperations.digitPermutation("STARTEA"));
		assertNull(VICOperations.digitPermutation("START"));
		assertEquals("1528069374", VICOperations.digitPermutation("2527058253"));

		
	}

	/**
	 * Test for improper data in the two string args.
	 */
	@Test
	public void testStraddlingCheckerboardImproperInput() {
		VICOperations.straddlingCheckerboard("0123456780", "A TIN SHOE");
		assertNull(VICOperations.straddlingCheckerboard("012345678A",
				"A TIN SHOE"));
		assertNull(VICOperations.straddlingCheckerboard("0123456789",
				"A TIN SH OE"));
		assertNull(VICOperations.straddlingCheckerboard("0123456789",
				"A TIN S3OE"));
		assertNull(VICOperations.straddlingCheckerboard("0123456789",
				"A TIN SH OE"));
		assertNull(VICOperations.straddlingCheckerboard("0123456789",
				"A T IN SHE"));
		assertNull(VICOperations.straddlingCheckerboard("0123456789",
				"A TIN SHE"));
		assertNull(VICOperations.straddlingCheckerboard("0123456789",
				"ATIN SHOE"));
		assertNull(VICOperations.straddlingCheckerboard("0123456780",
				"A TIN SHOE"));

	}

	/**
	 * Test proper output of testStraddlingCheckerboard, both strings are valid
	 */
	@Test
	public void testStraddlingCheckerboardProperInput() {
		ArrayList<String> test = VICOperations.straddlingCheckerboard(
				"4071826395", "A TIN SHOE");
		assertEquals("4", test.get(0));
		assertEquals("04", test.get(1));
		assertEquals("00", test.get(2));
		assertEquals("07", test.get(3));
		assertEquals("5", test.get(4));
		assertEquals("01", test.get(5));
		assertEquals("08", test.get(6));
		assertEquals("3", test.get(7));
		assertEquals("1", test.get(8));
		assertEquals("02", test.get(9));
		assertEquals("06", test.get(10));
		assertEquals("03", test.get(11));
		assertEquals("09", test.get(12));
		assertEquals("8", test.get(13));
		assertEquals("9", test.get(14));
		assertEquals("05", test.get(15));
		assertEquals("24", test.get(16));
		assertEquals("20", test.get(17));
		assertEquals("6", test.get(18));
		assertEquals("7", test.get(19));
		assertEquals("27", test.get(20));
		assertEquals("21", test.get(21));
		assertEquals("28", test.get(22));
		assertEquals("22", test.get(23));
		assertEquals("26", test.get(24));
		assertEquals("23", test.get(25));

	}

	/**
	 * Test proper output of testStraddlingCheckerboard, both strings are valid
	 */
	@Test
	public void testStraddlingCheckerboardProperInputTwo() {
		ArrayList<String> test = VICOperations.straddlingCheckerboard(
				"0123456789", "AT ONE SIR");
		assertEquals("0", test.get(0));
		assertEquals("20", test.get(1));
		assertEquals("21", test.get(2));
		assertEquals("22", test.get(3));
		assertEquals("5", test.get(4));
		assertEquals("23", test.get(5));
		assertEquals("24", test.get(6));
		assertEquals("25", test.get(7));
		assertEquals("8", test.get(8));
		assertEquals("26", test.get(9));
		assertEquals("27", test.get(10));
		assertEquals("28", test.get(11));
		assertEquals("29", test.get(12));
		assertEquals("4", test.get(13));
		assertEquals("3", test.get(14));
		assertEquals("60", test.get(15));
		assertEquals("61", test.get(16));
		assertEquals("9", test.get(17));
		assertEquals("7", test.get(18));
		assertEquals("1", test.get(19));
		assertEquals("62", test.get(20));
		assertEquals("63", test.get(21));
		assertEquals("64", test.get(22));
		assertEquals("65", test.get(23));
		assertEquals("66", test.get(24));
		assertEquals("67", test.get(25));

	}

	/**
	 * Test proper output of testStraddlingCheckerboard, both strings are valid
	 */
	@Test
	public void testStraddlingCheckerboardProperInputThree() {
		ArrayList<String> test = VICOperations.straddlingCheckerboard(
				"0987654321", "MATH CODE ");
		assertEquals("9", test.get(0));
		assertEquals("60", test.get(1));
		assertEquals("5", test.get(2));
		assertEquals("3", test.get(3));
		assertEquals("2", test.get(4));
		assertEquals("69", test.get(5));
		assertEquals("68", test.get(6));
		assertEquals("7", test.get(7));
		assertEquals("67", test.get(8));
		assertEquals("66", test.get(9));
		assertEquals("65", test.get(10));
		assertEquals("64", test.get(11));
		assertEquals("0", test.get(12));
		assertEquals("63", test.get(13));
		assertEquals("4", test.get(14));
		assertEquals("62", test.get(15));
		assertEquals("61", test.get(16));
		assertEquals("10", test.get(17));
		assertEquals("19", test.get(18));
		assertEquals("8", test.get(19));
		assertEquals("18", test.get(20));
		assertEquals("17", test.get(21));
		assertEquals("16", test.get(22));
		assertEquals("15", test.get(23));
		assertEquals("14", test.get(24));
		assertEquals("13", test.get(25));
	}
	
	/**
	 * Tests were provided by TA
	 */
	
	@Test
	public void test4() {
		
		System.out.println(VICOperations.straddlingCheckerboard("4071826395", "A TIN SHOE"));
	    assertNull(VICOperations.straddlingCheckerboard("4071826395", "ATIN SHOE"));
	    assertNull(VICOperations.straddlingCheckerboard("4071826395", "ATTIN SHOE"));
	    assertNull(VICOperations.straddlingCheckerboard("4071826395", "A TIN S OE"));
	    assertNull(VICOperations.straddlingCheckerboard("4071826395", "A TIN S OEEWR"));
	    System.out.println(VICOperations.straddlingCheckerboard("4071826395", "A TIN SHOZ"));
	    assertNull(VICOperations.straddlingCheckerboard("4071826395", "# T@N 43OZ"));
	    assertNull(VICOperations.straddlingCheckerboard("4071826393334", "A TIN SHOE"));
	    assertNull(VICOperations.straddlingCheckerboard("40718263 3", "A TIN SHOE"));
	    assertNull(VICOperations.straddlingCheckerboard("40%1@263#3", "A TIN SHOE"));
	    System.out.println(VICOperations.straddlingCheckerboard("6179204358", "A TIN SHOE"));
	    assertNull(VICOperations.straddlingCheckerboard("1111111111", "A IIN SHOE"));
	    assertNull(VICOperations.straddlingCheckerboard("0000000000", "I AM CODE"));
	}
	

}
